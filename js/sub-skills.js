/**
 * Sub-skills and skills info module.
 * handle the modale behaviors.
 */
var SubSkills = (function(my) {
    my.asideElem = document.querySelector('aside#subskills');
    my.titleElem = document.querySelector('aside#subskills h1');
    my.subskillsElem = document.querySelector('aside#subskills p#subskills');
    my.certificationElem = document.querySelector('aside#subskills p#certification');
    my.closesubskillsElem = document.querySelector('aside#subskills button#closesubskills');
    my.buttonSignOutElem = document.querySelector('button#signout-button');
    
    /**
     * Close the modal
     * @returns {void}
     */
    my.close = function () {
        my.asideElem.style.left = '-25vw';
        setTimeout(function () {
            my.buttonSignOutElem.style.visibility = 'visible';
            my.titleElem.innerHTML = '';
            my.subskillsElem.innerHTML = '';
            my.certificationElem.innerHTML = '';
        }, 300);
    }
    
    /**
     * Update modal's data with the given data.
     * @param {*} data 
     * @returns {void}
     */
    my.toggle = function (data) {
        my.buttonSignOutElem.style.visibility = 'hidden';
        if (my.asideElem.style.left && my.asideElem.style.left != '-25vw') {
            my.asideElem.style.left = '-5vw';
        }
        setTimeout(function () {
            my.asideElem.style.left= 0;
            my.titleElem.innerHTML = data.name ? data.name : 'donnée indisponible';
            my.subskillsElem.innerHTML = data.subSkills ? data.subSkills : 'donnée indisponible';
            my.certificationElem.innerHTML = data.cert ? data.cert : 'donnée indisponible';
        }, 300)
        
    }
    
    my.closesubskillsElem.onclick = my.close;
    return my;
})(SubSkills || {});