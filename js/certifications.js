/**
 * Certifications module.
 * Handles the certifications modal (navigation).
 */
const Certifications = (function (my) {
  my.data = [];
  my.certificationsElem = document.querySelector('aside#certifications');
  my.navElem = my.certificationsElem.querySelector('nav ul');
  my.cleanUpButton = document.querySelector('button#cleanFilters');
  my.openButton = document.querySelector('button#aside-open');
  my.closeButton = document.querySelector('button#aside-close');

  /**
   * Creates the modal. Sets up default state.
   * Registers onclick events (open/close the modal).
   * @param {Sunburst} Sunburst
   * @returns {void}
   */
  my.vroum = function (Sunburst) {
    my.data = Sunburst.certifications
    
    makeNav(Sunburst);
    
    my.openButton.style.display = 'block';
    my.cleanUpButton.style.display = 'block';
    my.cleanUpButton.onclick = my.cleanUp;
    
    my.openButton.onclick = my.toggle;
    my.closeButton.onclick = my.toggle;
  }

  /**
   * Iterates over all the certifications available.
   * Then, create clicable navigation elements.
   * Registers onclick events.
   * @param {*} Sunburst
   * @returns {void}
   */
  const makeNav = function (Sunburst) {
    my.data.forEach(function (cert) {
      const li = document.createElement("li");
      li.innerText = cert;
      li.onclick = certificationOnclick(Sunburst, cert);
      my.navElem.appendChild(li);
    });
  }

  /**
   * Certicifation onclick event handler
   * @param {Sunburst} Sunburst 
   * @param {String} cert 
   */
  const certificationOnclick = function (Sunburst, cert) {
    return function (e) {
      cleanActiveClass();
      const li = e.target
      li.className = 'active';
      Sunburst.path.style("opacity", function(d) {
        if (d.cert != cert) {
          return 0.2;
        }
      });
    }
  }

  /**
   * Util function. Clean class attributes of all certifications.
   * @returns {void}
   */
  const cleanActiveClass = function () {
    const lis = document.querySelectorAll('nav ul li');
    lis.forEach(function (li) {
      li.className = '';
    })
  }

  my.stop = function () {
    my.navElem.innerHTML = null;
    my.cleanUpButton.style.display = 'none';
    my.openButton.style.display = 'none';
    my.certificationsElem.style.right = '-20vw';
  }

  my.cleanUp = function () {
    my.cleanActiveClass()
    Sunburst.path.style("opacity", 1);
  }

  my.toggle = function () {    
    if (my.openButton.style.display !== 'none') {
      my.open();
    } else {
      my.close();
    }
  }

  my.open = function () {
    my.openButton.style.display = 'none';
    my.closeButton.style.display = 'block';
    my.certificationsElem.style.right = '0';
  }

  my.close = function () {
    setTimeout(function () {
      my.openButton.style.display = 'block';
    }, 300);
    my.closeButton.style.display = 'none';
    my.certificationsElem.style.right = '-20vw';
  }

  return my;
})({});
