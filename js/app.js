/**
 * Application module
 */
const App = (function (my) {

  /**
   * Registers callbacks & events
   * Calling this method will "start" the app
   * @returns {void}
   */
  my.vroum = function () {
    GoogleConnect.registerOnAuthorizedCallback(Sunburst.vroum);
    GoogleConnect.registerOnSignoutCallback(Sunburst.stop);

    exitModalsOn();
  }

  /**
   * Registers events that trigger modals closing
   * @returns {void}
   */
  const exitModalsOn = function () {
    document.addEventListener('keypress', event => {
      const keyName = event.key;
      if (keyName === 'Escape') closeModals();
    });
    document.querySelector('main').addEventListener('click', closeModals);
  }

  /**
   * Closes all the opened modals
   * 
   * @param {*} event, expects a object of type DOM event
   * @returns {void}
   */
  const closeModals = function (event) {
    if (event && event.target && event.target.id != 'sunburst') {
      return;
    }
    SubSkills.close();
    Certifications.close();
  }

  return my;
})({}, Sunburst, GoogleConnect, Cache);
