/**
 * Sunburst module. 
 * Uses D3.js
 */
const Sunburst = (function (my) {
  my.width = 600;
  my.height = 600;
  my.radius = (Math.min(my.width, my.height) / 2);

  my.formatNumber = d3.format(",d");
  my.x = d3.scale.linear()
        .range([0, 2 * Math.PI]);
  my.y = d3.scale.linear()
        .range([0, my.radius]);

  my.color = d3.scale.category20c();
  my.partition = d3.layout.partition()
                .value(function (d) { return d.size; });
  my.arc = d3.svg.arc()
          .startAngle(function (d) { return Math.max(0, Math.min(2 * Math.PI, my.x(d.x))); })
          .endAngle(function (d) { return Math.max(0, Math.min(2 * Math.PI, my.x(d.x + d.dx))); })
          .innerRadius(function (d) { return Math.max(0, my.y(d.y)); })
          .outerRadius(function (d) { return Math.max(0, my.y(d.y + d.dy)); });
  
  my.data = {
    name: "compétences",
    children: []
  };
  my.certifications = [];
  my.subSkills = [];

  my.svg = null;
  my.g = null;
  my.path = null;

  my.title = document.querySelector('header h1');

  /**
   * Creates the sunburst plot with the given data.
   * @param {*} googleData 
   */
  my.vroum = function (googleData) {
    my.title.style.display = 'none';
    formatGoogleData(googleData);

    my.svg = d3.select("section#sunburst")
            .append("div")
            .classed("svg-container", true)
            .append("svg")
            .attr("preserveAspectRatio", "xMinYMin meet")
            .attr("viewBox", "-300 -300 600 600")
            .append("g")

    my.g = my.svg.selectAll("g")
      .data(my.partition.nodes(my.data))
      .enter().append("g")

    my.path = my.g.append("path")
      .attr("d", my.arc)
      .style("fill", function (d) { return my.color((d.children ? d : d.parent).name); });

    my.g.append("text")
      .attr("transform", function(d) { return "rotate(" + computeTextRotation(d) + ")"; })
      .attr("x", function(d) { return my.y(d.y); })
      .attr("dx", "1") // margin
      .attr("dy", ".35em") // vertical-align
      .style("font-size", ".7rem")
      .text(function(d) { return d.name; });

    my.g.on('click', function (d) {
      SubSkills.toggle(d)
    })
      
    // d3.select(self.frameElepathment).style("height", my.height + "px");

    Certifications.vroum(my);
  }

  const computeTextRotation = function (d) {
    return (my.x(d.x + d.dx / 2) - Math.PI / 2) / Math.PI * 180;
  }
 
  my.stop = function() {
    const sunburstElem = document.querySelector('svg');
    
    if (sunburstElem) sunburstElem.remove();
    
    my.data = {
      name: "compétences",
      children: []
    };
    
    my.title.style.display = 'block';
    Certifications.stop();
    
  }

  const formatGoogleData = function (googleData) {
    const result = googleData.valueRanges;
    const skills = result[1].values;
    const blocs = result[0].values;
    const certifications = result[2].values;
    const subSkills = result[3].values;
    
    formatCertifications(certifications);
    formatSkillBlocs(blocs);
    formatSkills(skills, blocs, certifications, subSkills);
  }

  const formatCertifications = function (certifications) {
    certifications.forEach(function (cert) {
      const certName = cert[0];
      if (!my.certifications.includes(certName)) {
        my.certifications.push(certName);
      }
    });
  }

  const formatSkillBlocs = function (blocs) {
    blocs.forEach(function (bloc) {
      const blocName = bloc[0];
      if (!dataContainsSkillBloc(blocName)) {
        my.data.children.push({ name: blocName, children: [] });
      }
    });
  }

  const formatSkills = function (skills, blocs, certifications, subSkills) {
    skills.forEach(function (skill, index) {
      const skillName = skill[0];
      const skillBloc = blocs[index][0];
      const certification = certifications[index][0];
      const subSkill = subSkills[index][0];

      blocIndex = my.data.children.findIndex(function (bloc) {
        return bloc.name == skillBloc;
      });

      my.data.children[blocIndex].children.push({ 
        name: skillName, 
        size: 3, 
        cert: certification,
        subSkills: subSkill
      });
    });
  }

  const dataContainsSkillBloc = function (blocName) {
    return my.data.children.filter(function (bloc) {
      return bloc.name == blocName;
    }).length
  }

  return my;
})({}, Certifications);