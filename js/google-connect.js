/**
 * Google OAuth2 Module.
 * Consumes Google's Spreadsheets REST API.
 */
const GoogleConnect = (function (my) {
  // Client ID and API key from the Developer Console
  my.CLIENT_ID = '345999953857-m91f5b84mnmvv115i66ctpr8grogvh99.apps.googleusercontent.com';
  my.API_KEY = 'AIzaSyCexhltWl4li6oLCLGf5NHEB5e6YuaWWmE';

  // Array of API discovery doc URLs for APIs used by the quickstart
  my.DISCOVERY_DOCS = ["https://sheets.googleapis.com/$discovery/rest?version=v4"];

  // Authorization scopes required by the API; multiple scopes can be
  // included, separated by spaces.
  my.SCOPES = "https://www.googleapis.com/auth/spreadsheets.readonly";

  my.authorizeButton = document.getElementById('authorize-button');
  my.signoutButton = document.getElementById('signout-button');

  my.onAuthorizedCallback = function () {};
  my.onSignoutCallback = function () {};

  /**
   * On load, called to load the auth2 library and API client library.
   * @returns {void}
   */
  my.handleClientLoad = function () {
    gapi.load('client:auth2', my.initClient);
  }

  /**
   * Initializes the API client library and sets up sign-in state
   * listeners.
   * @returns {void}
   */
  my.initClient = function () {
    gapi.client.init({
      apiKey: my.API_KEY,
      clientId: my.CLIENT_ID,
      discoveryDocs: my.DISCOVERY_DOCS,
      scope: my.SCOPES
    }).then(function () {
      // Listen for sign-in state changes.
      gapi.auth2.getAuthInstance().isSignedIn.listen(my.updateSigninStatus);

      // Handle the initial sign-in state.
      my.updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
      my.authorizeButton.onclick = my.handleAuthClick;
      my.signoutButton.onclick = my.handleSignoutClick;
    });
  }

  /**
   * Called when the signed in status changes, to update the UI
   * appropriately. After a sign-in, the API is called.
   * @param {boolean} isSignedIn
   * @returns {void}
   */
  my.updateSigninStatus = function (isSignedIn) {
    if (isSignedIn) {
      my.authorizeButton.style.display = 'none';
      my.signoutButton.style.display = 'block';
      my.skills();
    } else {
      my.authorizeButton.style.display = 'block';
      my.signoutButton.style.display = 'none';
      my.onSignoutCallback();
    }
  }

  /**
   * Sign in the user upon button click.
   * @returns {void}
   */
  my.handleAuthClick = function () {
    gapi.auth2.getAuthInstance().signIn();
  }

  /**
   * Sign out the user upon button click.
   * @returns {void}
   */
  my.handleSignoutClick = function () {
    gapi.auth2.getAuthInstance().signOut();
  }

  /**
   * Registers callback.
   * @param {Function} cb 
   * @returns {void}
   */
  my.registerOnAuthorizedCallback = function (cb) {
    if (typeof cb !== 'function') throw new Error('a callback must be a function');
    my.onAuthorizedCallback = cb
  }

  /**
   * Registers callback.
   * @param {Function} cb 
   * @returns {void}
   */
  my.registerOnSignoutCallback = function (cb) {
    if (typeof cb !== 'function') throw new Error('a callback must be a function');
    my.onSignoutCallback = cb
  }

  /**
   * Gets the data from Google Spreadsheet.
   * On Google's response, calls the registered callback for the on authorized hook.
   * @returns {void}
   */
  my.skills = function () {
    if (Cache.exists()) {
      my.onAuthorizedCallback(Cache.get());
      return;
    }

    gapi.client.sheets.spreadsheets.values.batchGet({
      spreadsheetId: '1TZwl4svQ-2m_zMLpXpvqtpD98JqRX7vZlw4zKiKrOAk',
      ranges: ['E2:E75', 'I2:I75', 'B2:B75', 'J2:J75'],
    }).then(function (response) {
      Cache.set(response.result);
      my.onAuthorizedCallback(response.result);
    }, function (response) {
      throw new Error('Error: ' + response.result.error.message);
    });
  }

  return my;
})({});