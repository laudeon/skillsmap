const CACHE_LIFE = 3600000;
const CACHE_NAME = 'skillsmap';
const BASE_CACHE = {
    datetime: null,
    data: null
};

/**
 * Caching module using localStorage.
 */
const Cache = (function (my) {
    my.cache = BASE_CACHE;

    /**
     * Sets my.cache with the existing cache or the default cache.
     * Existing cache comes from local storage.
     * @returns {void}
     */
    my.fetch = function () {
        const cache = localStorage.getItem(CACHE_NAME);
        
        if (cache === 'undefined' || !cache) {
            my.cache = BASE_CACHE;
        } else {
            my.cache = JSON.parse(cache);
        }
    }

    /**
     * Sets my.cache with the given data.
     * Saves my.cache in local storage.
     * @param {*} data
     * @returns {void}
     */
    my.set = function (data) {
        my.cache.data = data;
        my.cache.datetime = (new Date()).getTime();

        localStorage.removeItem(CACHE_NAME);
        localStorage.setItem(CACHE_NAME, JSON.stringify(my.cache));
    }

    /**
     * Check if there are already non expired cache data in local storage.
     * @returns {boolean}
     */
    my.exists = function () {
        const timeNow = (new Date()).getTime();

        my.fetch();

        if (!my.cache.data || !my.cache.datetime) return false;
        if (timeNow >= (my.cache.datetime + CACHE_LIFE)) return false;

        return true;
    }

    /**
     * Gets the actual cached data.
     * @returns {*}
     */
    my.get = function () {
        if (!my.cache.data) my.fetch();
        
        return my.cache.data;
    }

    return my;
})({});
